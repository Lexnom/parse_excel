import psycopg2
import config
import pandas as pd
import os

def get_connet():
    return psycopg2.connect(host=config.HOST_DB, database=config.DBNAME, user=config.LOGIN_BD, password=config.PASS_BD)

def close_connect(con):
    con.close()

def insert_file(file):
    connect = get_connet()
    list_obj = []
    excl = pd.ExcelFile(file.name)

    path_file = os.path.abspath(file.name)
    with open(path_file, 'rb') as f:
        new_file = f.read()
        data = excl.parse(excl.sheet_names[0])
        for item in data.columns.ravel():

            list_obj.append(data[item].tolist())

        query = ("INSERT INTO excel_files(file) VALUES (%s) RETURNING id;") % (psycopg2.Binary(new_file))
        f.close()
    cur = connect.cursor()
    cur.execute(query)
    connect.commit()
    id_file = cur.fetchone()[0]
    file.close()
    return list_obj, id_file

def insert_data(list_obj, id_file):
    connect = get_connet()
    cur = connect.cursor()
    person_list = []

    i = 0

    while i < len(list_obj[0]):
        list_person_info = []
        for item in list_obj:
            list_person_info.append(item[i])
        person_list.append(list_person_info)
        i += 1
    for info_person in person_list:
        print(info_person)
        query = ("INSERT INTO data_in_excel(id_file_excel, person, birthday, height, weight, presence_of_children, gender, wesidence, inn_person, wages) VALUES "
                 "('%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s')") % (id_file, info_person[0], info_person[1], info_person[2],info_person[3],info_person[4], info_person[5],
                                                                                   info_person[6],info_person[7], info_person[8])
        cur.execute(query)
    connect.commit()
    close_connect(connect)
