import mail
import config
import connect_bd

if __name__ == '__main__':
    file = mail.get_file_in_message(config.IMAP_HOST, config.PORT, config.USER, config.PASSWORD)
    if file:
        list_obj, id = connect_bd.insert_file(file)
        connect_bd.insert_data(list_obj, id)
    else:
        print('Нет файлов')