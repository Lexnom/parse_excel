#!/usr/bin/env python

# -*- coding: utf-8 -*-


from imaplib import IMAP4_SSL
import email
import base64


def get_file_in_message(_HOST, _PORT, _LOGIN, _PASS):

    connection = IMAP4_SSL(host=_HOST, port=_PORT)
    connection.login(user=_LOGIN, password=_PASS)

    connection.select('INBOX')
    typ, data = connection.search('utf-8', '(SUBJECT "Тестовое задание")'.encode('utf-8'))
    data_id = data[0].split()
    if data_id:
        typ, message_data = connection.fetch(data_id[-1], '(RFC822)')

        mail = email.message_from_bytes(message_data[0][1])

        if mail.is_multipart():
            for part in mail.walk():
                filename = part.get_filename()
                if filename:
                    print(filename)
                    #name_file = base64.b64decode(filename.split('?')[3]).decode('utf-8')

                    with open(filename, 'wb') as new_file:
                        new_file.write(part.get_payload(decode=True))

                    return new_file

    connection.close()
    connection.logout()